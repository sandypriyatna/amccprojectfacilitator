package com.tupaiaer.amcc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val getNim = intent.getStringExtra("NIM")
        val getPass =intent.getStringExtra("PASS")

        Log.d("NIM", getNim)
        Log.d("PASS", getPass)

        tv_nim.text = getNim
        tv_password.text = getPass
    }
}
