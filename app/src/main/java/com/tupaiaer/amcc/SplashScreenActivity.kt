package com.tupaiaer.amcc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : AppCompatActivity() {

    var SPLASH_TIME = 10000.toLong()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        iv_splash.playAnimation()
        iv_splash.loop(true)

        Handler().postDelayed(
            {
                startActivity(Intent(Intent(this@SplashScreenActivity, LoginActivity::class.java)))
                finish()
            }, SPLASH_TIME
        )
    }
}
