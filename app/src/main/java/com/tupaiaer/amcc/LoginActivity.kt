package com.tupaiaer.amcc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        btn_login.setOnClickListener {
            var nim = et_nim.text.toString()
            var password = et_password.text.toString()

            var intent = Intent(Intent(this@LoginActivity, MainActivity::class.java))
            intent.putExtra("NIM", nim)
            intent.putExtra("PASS", password)
            startActivity(intent)
        }
    }
}
